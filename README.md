
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

API Rest que simula el manejo de tarjetas de crédito.    


## Instalación

```bash
$ yarn install
```

## Ejecución

```bash
# Desarrollo
$ yarn run start

# Modo Watch
$ yarn run start:dev

# TEST Unit
$ yarn run test

# TEST e2e
$ yarn run test:e2e

```

## End points

```bash
# Swagger
/api

# Agrega una nueva tarjeta
POST
/api/cards

# Obtiene la lista de las tarjetas agregadas
GET
/api/cards

```

## Tecnologías Utilizadas

- [Nest Js](https://nestjs.com/) (Javascript que usa Express)

## Prueba en vivo

[API Card Service by Daniel Pérez](https://card-service-api.herokuapp.com/api/)

