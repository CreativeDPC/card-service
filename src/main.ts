import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const config = new DocumentBuilder()
    .setTitle('Card Service')
    .setDescription('Servicio que maneja los servicios relacioinados con tarjetas de credito.')
    .setContact("Daniel Pérez Cabrera, Cel: 55 1120 0160, Email: creativedesing.dpc@gmail.com","", "creativedesing.dpc@gmail.com")
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(process.env.PORT || '3000');
}
bootstrap();
