import { ApiProperty } from "@nestjs/swagger";

export class Card {
    @ApiProperty()
    cardNumber: string;
    @ApiProperty()
    cardName: string;
    @ApiProperty()
    cvv: string;
    @ApiProperty()
    expiration: string;

    constructor(data:any = {}){
        this.cardNumber = data.cardNumber || '';
        this.cardName = data.cardName || '';
        this.cvv = data.cvv || '';
        this.expiration = data.expiration || '';
    }
}
