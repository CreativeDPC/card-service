import { Controller, Get, Post, Body, HttpException, HttpStatus, HttpCode } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { CardsService } from './cards.service';
import { CreateCardDto } from './dto/create-card.dto';
import { Card } from './entities/card.entity';

@ApiTags('Cards')
@Controller('api/cards')
export class CardsController {
  constructor(private readonly cardsService: CardsService) {}

  @ApiOperation({ summary: 'Da de alta una nueva tarjeta de crédito.' })
  @Post()
  @ApiResponse({ status: 200, description: 'succes', type:Card}) 
  @HttpCode(200)
  async create(@Body() createCardDto: CreateCardDto) {
    try {
      await this.cardsService.create(createCardDto)
    } catch (error) {
      return new HttpException(error, HttpStatus.BAD_REQUEST);
    }

    return {
        statusCode: HttpStatus.OK,
        message: 'success'
      }
  }

  @ApiOperation({ summary: 'Obtiene todas las tarjetas de crédito.' })
  @Get()
  @ApiResponse({ status: 200, description: 'succes', type:Card}) 
  async findAll() {
    return this.cardsService.findAll();
  }

}
