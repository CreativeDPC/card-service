import { Test, TestingModule } from '@nestjs/testing';
import { CardsController } from './cards.controller';
import { CardsService } from './cards.service';
import { Card } from './entities/card.entity';

describe('CardsController', () => {
  let controller: CardsController;
  let cardsService: CardsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CardsController],
      providers: [CardsService],
    }).compile();

    cardsService = module.get<CardsService>(CardsService);
    controller = module.get<CardsController>(CardsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of cards', async () => {
      const result = [new Card({
        cardNumber: '4242424242424242',
        cardName: 'Albert Damon',
        cvv: '123',
        expiration: 'MM-YYYY'
        })];
      jest.spyOn(cardsService, 'findAll').mockImplementation(async () => result);

      expect(await controller.findAll()).toBe(result);
    });
  });
});
