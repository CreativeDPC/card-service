import { PartialType } from '@nestjs/swagger';
import { Card } from '../entities/card.entity';

export class CreateCardDto extends PartialType(Card) {}
