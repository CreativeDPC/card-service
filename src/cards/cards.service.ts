import { Injectable } from '@nestjs/common';
import { CreateCardDto } from './dto/create-card.dto';
import { Card } from './entities/card.entity';

@Injectable()
export class CardsService {
  private cards: Array<Card> = [];

  async create(createCardDto: CreateCardDto) {
    for (const property in new Card()) {
        if(!createCardDto[property] || createCardDto[property] === '') throw new Error(`Invalid property ${property}`);              
    }
    this.cards = [...this.cards, new Card(createCardDto)];
  }

  async findAll():Promise<Array<Card>> {
    return this.cards;
  }

}
